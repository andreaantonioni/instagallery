//
//  InstagramPhotoCell.swift
//  InstaGallery
//
//  Created by Andrea Antonioni on 09/06/16.
//  Copyright © 2016 Andrea Antonioni. All rights reserved.
//

import UIKit

class InstagramPhotoCell: UICollectionViewCell {

    // MARK: Properties
    @IBOutlet weak var imageView: UIImageView!
    
}
