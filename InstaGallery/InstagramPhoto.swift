//
//  InstagramPhoto.swift
//  InstaGallery
//
//  Created by Andrea Antonioni on 09/06/16.
//  Copyright © 2016 Andrea Antonioni. All rights reserved.
//

import Foundation
import UIKit

class InstagramPhoto {
    
    private(set) var url: String
    private(set) var id: String
    private(set) var thumbnail: UIImage
    
    init(url: String, id: String) {
        self.url = url
        self.id = id
        
        let imageData = NSData(contentsOfURL: NSURL(string: url)!)
        self.thumbnail = UIImage(data: imageData!)!
    }
}