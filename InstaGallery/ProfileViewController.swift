//
//  ProfileViewController.swift
//  InstaGallery
//
//  Created by Andrea Antonioni on 10/06/16.
//  Copyright © 2016 Andrea Antonioni. All rights reserved.
//

import UIKit

class ProfileViewController: UITableViewController {

    // MARK: Properties
    @IBOutlet weak var avatarView: UIImageView!
    @IBOutlet weak var fullName: UILabel!
    @IBOutlet weak var username: UILabel!
    @IBOutlet weak var bio: UILabel!
    
    override func viewDidLoad() {
        let user = Instagram.user!
        
        avatarView.image = user.avatarImage
        fullName.text = user.fullName
        username.text = user.username
        bio.text = user.bio
    }
    
    // MARK: Actions
    @IBAction func logout(sender: UIButton) {
        let actionSheet = UIAlertController(title: "Are you sure to logout?", message: "", preferredStyle: UIAlertControllerStyle.ActionSheet)
        
        let logoutAction = UIAlertAction(title: "Logout", style: UIAlertActionStyle.Destructive, handler: {(paramAction:UIAlertAction!) in
                                            
            //Delete Cookies
            let storage : NSHTTPCookieStorage = NSHTTPCookieStorage.sharedHTTPCookieStorage()
            for cookie in storage.cookies! {
                storage.deleteCookie(cookie)
            }
            
            //Delete access token
            NSUserDefaults.standardUserDefaults().removeObjectForKey("accessToken")
            NSUserDefaults.standardUserDefaults().synchronize()
            
            //Loading LoginViewController
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let tutorialController = storyboard.instantiateViewControllerWithIdentifier("tutorialViewController")
            tutorialController.modalTransitionStyle = UIModalTransitionStyle.FlipHorizontal
            self.presentViewController(tutorialController, animated: true, completion: nil)
            
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Default, handler: {(paramAction:UIAlertAction!) in
            return
        })
        
        actionSheet.addAction(logoutAction)
        actionSheet.addAction(cancelAction)
        
        presentViewController(actionSheet, animated: true, completion: nil)
    }
}
