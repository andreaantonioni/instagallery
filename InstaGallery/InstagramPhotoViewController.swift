//
//  InstagramPhotoViewController.swift
//  InstaGallery
//
//  Created by Andrea Antonioni on 09/06/16.
//  Copyright © 2016 Andrea Antonioni. All rights reserved.
//

import UIKit

class InstagramPhotoViewController: UIViewController {

    // MARK: Properties
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var imageView: UIImageView!
    var instagramPhoto: InstagramPhoto?
    
    override func viewDidLoad() {
        
        dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0)) { [unowned self] in
            
            do {
                self.activityIndicator.startAnimating()
                let url = try Instagram.getUrlLargeMediaRequest(self.instagramPhoto!.id)
                let imageData = NSData(contentsOfURL: url)
                
                dispatch_async(dispatch_get_main_queue()) {
                    self.activityIndicator.stopAnimating()
                    self.imageView.image = UIImage(data: imageData!)!
                }
                
            } catch InstagramError.AuthorizationRejected {
                dispatch_async(dispatch_get_main_queue()) {
                    self.activityIndicator.stopAnimating()
                    
                    let alertController = UIAlertController(title: "Authorization Rejected", message:
                        "Maybe you have revoke our access to you Instagram account. Please sign in again.", preferredStyle: UIAlertControllerStyle.Alert)
                    
                    alertController.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default,handler: { (alertController) -> Void in
                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                        let vc = storyboard.instantiateViewControllerWithIdentifier("tutorialViewController")
                        
                        self.presentViewController(vc, animated: false, completion: nil)
                    }))
                    
                    self.presentViewController(alertController, animated: true, completion: nil)
                }
            } catch {
                dispatch_async(dispatch_get_main_queue()) {
                    self.activityIndicator.stopAnimating()
                    
                    let alertController = UIAlertController(title: "Something went wrong", message:
                        "Something went wrong. Check your internet connection", preferredStyle: UIAlertControllerStyle.Alert)
                    
                    alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.Default, handler: nil))
                    
                    self.presentViewController(alertController, animated: true, completion: nil)
                }
            }
            
            
            
        }
        
    }
}
