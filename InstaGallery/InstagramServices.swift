//
//  ViewController.swift
//  InstaGallery
//
//  Created by Andrea Antonioni on 09/06/16.
//  Copyright © 2016 Andrea Antonioni. All rights reserved.
//

import UIKit
import Just
import SwiftyJSON

private let clientID: String = "79cf665469f64579b871e626c5c9aa66"
private let clientSecret: String = "78494d9086c14b2aab30a24608d50977"
private let redirectURI: String = "https://localhost/"

enum InstagramError: ErrorType {
    case AuthorizationRejected
    case NetworkProblem
}

class InstagramUser {
    
    private(set) var avatarImage: UIImage
    private(set) var fullName: String
    private(set) var username: String
    private(set) var bio: String
    
    init(accessToken: String) throws {
        
        let url = "https://api.instagram.com/v1/users/self/?access_token=" + accessToken
        let response = Just.get(url)
        if !response.ok {
            if response.json != nil {
                throw InstagramError.AuthorizationRejected
            }
            throw InstagramError.NetworkProblem
        }
        
        let json = JSON(response.json!)
        let informations = json["data"]
        let urlProfilePicture = NSURL(string: informations["profile_picture"].string!)!
        self.avatarImage = UIImage(data: NSData(contentsOfURL: urlProfilePicture)!)!
        self.fullName = informations["full_name"].string!
        self.username = informations["username"].string!
        self.bio = informations["bio"].string!
    }
}

class Instagram: NSObject {
    
    // MARK Properties
    static var images: [InstagramPhoto]?
    static var user: InstagramUser?
    private static var accessToken: String? {
        didSet{
            saveUser()
        }
    }
    
    // MARK: Setup
    static func loadResources() throws {
        do{
            try self.user = InstagramUser(accessToken: accessToken!)
            try downloadRecentMedia()
        } catch let error{
            throw error
        }
    }
    
    // MARK: HTTP Requests
    static func downloadRecentMedia() throws {
        let url = "https://api.instagram.com/v1/users/self/media/recent/?access_token=" + accessToken!
        let response = Just.get(url)
        
        if !response.ok {
            if response.json != nil {
                throw InstagramError.AuthorizationRejected
            }
            throw InstagramError.NetworkProblem
        }
        
        let json = JSON(response.json!)
        let images = json["data"]
        var instagramImages = [InstagramPhoto]()
        for (_,subJson):(String, JSON) in images where subJson["type"].string == "image"{
            let id = subJson["id"].string!
            let images = subJson["images"]
            instagramImages += [InstagramPhoto(url: images["thumbnail"]["url"].string!, id: id)]
        }
        
        self.images = instagramImages
    }
    
    static func getUrlLargeMediaRequest(mediaID: String) throws -> NSURL {
        let url = "https://api.instagram.com/v1/media/" + mediaID + "?access_token=" + accessToken!
        let response = Just.get(url)
        
        if !response.ok {
            if response.json != nil {
                throw InstagramError.AuthorizationRejected
            }
            throw InstagramError.NetworkProblem
        }
        
        let json = JSON(response.json!)
        let image = json["data"]["images"]["low_resolution"]
        
        return NSURL(string: image["url"].string!)!
    }
    
    // MARK: Load & Save
    static func loadUser() -> Bool {
        let defaults = NSUserDefaults.standardUserDefaults()
        if let token = defaults.stringForKey("accessToken") {
            accessToken = token
            return true
        }
        return false
    }
    
    private static func saveUser() {
        let defaults = NSUserDefaults.standardUserDefaults()
        defaults.setObject(accessToken, forKey: "accessToken")
    }
}

class AuthenticationViewController: UIViewController, UIWebViewDelegate {

    // MARK: Properties
    @IBOutlet weak var authenticationWebView: UIWebView!
    
    // MARK: UIViewController
    override func viewDidLoad() {
        super.viewDidLoad()
        
        authenticationWebView.delegate = self
        
        let url = NSURL (string: "https://api.instagram.com/oauth/authorize/?client_id=" + clientID + "&redirect_uri=" + redirectURI + "&response_type=code")
        let request = NSURLRequest(URL: url!)
        authenticationWebView.loadRequest(request)
    }

    // MARK: UIWebViewDelegate
    func webView(webView: UIWebView, shouldStartLoadWithRequest request: NSURLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        
        let urlComponents = NSURLComponents(string: (request.URL?.absoluteString)!)
        let queryItems = urlComponents?.queryItems
        
        if queryItems?.filter({$0.name == "error"}).first != nil {
            print("ERROR")
            return false
        }
        else if let param = queryItems?.filter({$0.name == "code"}).first {
            print("AUTHORIZED")
            print("code: \(param.value!)")
            let code = param.value!
            let response = Just.post(
                "https://api.instagram.com/oauth/access_token",
                data: ["client_id": clientID, "client_secret": clientSecret, "grant_type": "authorization_code", "redirect_uri": redirectURI, "code": code]
            )
            let json = JSON(response.json!)
            Instagram.accessToken = json["access_token"].string!
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewControllerWithIdentifier("navigationViewController") as! UINavigationController
            self.presentViewController(vc, animated: true, completion: nil)
            print(response.json)
        }
        return true
    }

}

