//
//  CollectionViewController.swift
//  InstaGallery
//
//  Created by Andrea Antonioni on 09/06/16.
//  Copyright © 2016 Andrea Antonioni. All rights reserved.
//

import UIKit

class InstagramPhotosViewController: UICollectionViewController {

    // MARK: Properties
    
    private let reuseIdentifier = "InstagramCell"
    private var images = [InstagramPhoto]()
    
    // MARK: UICollectionViewController
    
    override func viewDidLoad() {
        images = Instagram.images!
    }
    
    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return images.count
    }
    
    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(reuseIdentifier, forIndexPath: indexPath) as! InstagramPhotoCell
        
        let instagramPhoto = images[indexPath.row]
        cell.imageView.image = instagramPhoto.thumbnail
        
        return cell
    }
    
    // MARK: Navigaton
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "showPhoto" {
            let instagramPhotoViewController = segue.destinationViewController as! InstagramPhotoViewController
            
            // Get the cell that generated this segue.
            if let selectedInstagramPhotoCell = sender as? InstagramPhotoCell {
                let indexPath = collectionView!.indexPathForCell(selectedInstagramPhotoCell)!
                let selectedPhoto = images[indexPath.row]
                instagramPhotoViewController.instagramPhoto = selectedPhoto
            }
        }
    }
}

// MARK: UICollectionViewDelegateFlowLayout

extension InstagramPhotosViewController : UICollectionViewDelegateFlowLayout {
    
    func collectionView(collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                               sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        
        let instagramPhoto =  images[indexPath.row]
        
        return instagramPhoto.thumbnail.size
    }
    
    func collectionView(collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                               insetForSectionAtIndex section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 50.0, left: 20.0, bottom: 50.0, right: 20.0)
    }
}
