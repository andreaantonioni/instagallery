# InstaGallery #

InstaGallery is a simple Swift application for iOS devices which allows you to sign in with your Instagram account and see all your photos.

### How to use ###
InstaGallery uses Instagram API and it is still in sandbox mode, so until InstaGallery will be accepted by Instagram, the only Instagram account that you can use to sign in in InstaGallery is this:

username: instagallery2016

pw: inst@g@llery2016

### Framework used ###

* [JustHTTP](https://github.com/JustHTTP/Just) to send HTTP requests and get responses
* [SwiftyJSON](https://github.com/SwiftyJSON/SwiftyJSON) to manage JSON in an easly way in Swift

And I also used [Carthage](https://github.com/Carthage/Carthage) as dependency manager

### Contact me ###

* Email: [andreaantonioni97gmail.com](mailto:andreaantonioni97gmail.com)
* [Linkedin](https://it.linkedin.com/in/andreaantonioni)